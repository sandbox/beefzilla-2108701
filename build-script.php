<?php
define('RELATIVE_DRUPAL_BASE_PATH', '../../../../');
define('YUI_LOCATION', 'sites/all/libraries/yuicompressor-'); #relative to the drupal base
define('YUI_VERSION', '2.4.6');
define('JS_LINE_COMMENT_PATTERN_FRAGMENT', '^\s*\/\/\s*');
define('MINIFY_DELETE_JS_START', 'MINIFY-DELETE-START');
define('MINIFY_DELETE_JS_END', 'MINIFY-DELETE-END');
define('MINIFY_DELETE_JS_START_PATTERN', '/' . JS_LINE_COMMENT_PATTERN_FRAGMENT . MINIFY_DELETE_JS_START . '/');
define('MINIFY_DELETE_JS_END_PATTERN', '/' . JS_LINE_COMMENT_PATTERN_FRAGMENT . MINIFY_DELETE_JS_END . '/');

# Operate as if called from drupal for better compatibility.

if(!chdir(RELATIVE_DRUPAL_BASE_PATH)){
	echo "Could not change to drupal base directory.\n";
	exit(2);
}

define('DRUPAL_ROOT', getcwd());

#prevent warnings
$_SERVER['SCRIPT_NAME'] = DRUPAL_ROOT . '/index.php';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

require_once 'includes/bootstrap.inc'; #give us some drupal functionality
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$GLOBALS["do-min-delete"] = TRUE;

function _stash_unlink_old_compressed_js($assetKey = '', $theme = NULL) {
	if($theme === NULL) {
		$theme = _stash_get_active_theme();
	}
	if($oldFile = _stash_get_compressed_js_file($assetKey, $theme)) {
		if(unlink($oldFile)) {
			_stash_alert("$oldFile removed.");
			return TRUE;
		}
		else {
			_stash_alert("could not remove $oldFile");
		}
	}
	return FALSE;
}

$GLOBALS["do-min-cat-verbose"] = FALSE;

function _stash_compress_js($assetKey = '', $theme = NULL) {

	$compressor = YUI_LOCATION.YUI_VERSION."/build/yuicompressor-".YUI_VERSION.".jar";
	if(!file_exists($compressor)) {
		_stash_alert("No compressor found at $compressor cannot compress JavaScript.");
		return FALSE;
	}

	if($theme === NULL) {
		$theme = _stash_get_active_theme();
	}
	list($assetKeyClass, $value, $assetExt) = _stash_split_asset_key($assetKey);
	if(_stash_is_compressed_js_current($assetKey, $theme)) {
		echo $theme . ($assetKey ? " $assetKey" : '') . " compressed js is current\n";
		return FALSE;
	}
	
	#if there is a _build_.txt file, give that priority over a simple js file
	if($inputFile = _stash_concatenate_js($assetKey, $theme)) {
		$removeConcatenated = TRUE;
	}
	else if($assetKeyClass != 'omni') {
		$inputFile = _stash_get_node_js_file($assetKey, $theme);
	}
	
	if($inputFile) {
		if($outputFile = _stash_current_compressed_js_file($assetKey, $theme)) {
			_stash_unlink_old_compressed_js($assetKey, $theme);
			$cmd = "java -jar $compressor ";
			if($GLOBALS["do-min-cat-verbose"]) {
				$cmd .= "--verbose ";
			}
			$cmd .= "--type js -o $outputFile $inputFile";

			if($result = shell_exec($cmd)) {
				echo "$result happened while trying to compress javascript with command:\n$cmd\n";
			}
			elseif(!file_exists($outputFile)) {
				echo "$outputFile does not exist, but it should!\n";
			}
			elseif(!filesize($outputFile)) {
				echo "$outputFile contains no data\n";
			}
			else {
				echo "$outputFile created :)\n";
				if(isset($removeConcatenated)){
					unlink($inputFile);
				}
				return TRUE;
			}
		}
		else {
			_stash_alert("Could not get outputFile name");
		}
	}

	_stash_alert("no js files found");
	return FALSE;
}

#######################
# COMMAND LINE SCRIPT #
#######################

$shortOpts = 'hvt:n:y:k:u:'; #help, theme <value>, node <value>, node-type <value>, asset-key <value>, unit <value>, verbose
$longOpts = array('help', 'no-min-delete', 'verbose');

$opts = getopt($shortOpts, $longOpts);

if(isset($opts['h'])) {
	$currentFileName = explode('/', __FILE__);
	$currentFileName = $currentFileName[count($currentFileName) - 1];
	
	echo "usage: php -q $currentFileName [options]\n" . 
	"Run without an argument to make a new compressed build of all javascript files used on every page, for the default theme.\n" .
	"Run with \"-n\" option, followed by a drupal node alias to make a compressed version of <theme>/inline/path/to/node/alias.js that is included inline only for that node.\n" .
	"Run with \"-t\" option, followed by a sub-theme name to compress js for a subtheme.\n" .
	"Run with \"-y\" option, followed by a node-type to compress js for a node-type\n" .
	"Run with \"-u\" option, followed by a unit name to compress js for a JavaScript unit\n" .
	"Run with \"-k\" option, followed by an asset-key, in the form of \"<key-type>:<value>\", to compress js by asset-key\n" .
	"Run with \"--no-min-delete\" option to prevent the min-cat process from deleting lines not meant for production\n" .
	"Run with \"--verbose\" option for more talking\n" .
	"See garlic theme settings to enable compressed javascript after creating a compressed build file.\n";
}
else {
	$theme = isset($opts['t']) ? $opts['t'] : _stash_get_active_theme();
	if(isset($opts['k'])){
		$assetKey = $opts['k'];
	}
	elseif (isset($opts['n'])){
		$assetKey = 'alias:' . $opts['n'];
	}
	elseif (isset($opts['y'])){
		$assetKey = 'type:' . $opts['y'];
	}
	elseif (isset($opts['u'])){
		$assetKey = 'unit:' . $opts['u'];
	}
	else{
		$assetKey = '';
	}

	if(isset($opts['no-min-delete'])) {
		$GLOBALS["do-min-delete"] = FALSE;
	}

	if(isset($opts['verbose']) || isset($opts['v'])) {
		$GLOBALS["do-min-cat-verbose"] = TRUE;
	}

	if(_stash_compress_js($assetKey, $theme)){
	exit(0);
	}

}

exit(1);